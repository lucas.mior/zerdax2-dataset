COLORS = {
    0: (120, 0, 210),
    1: (120, 0, 210),
    2: (0, 0, 255),
    3: (0, 140, 140),
    4: (0, 255, 0),
    5: (140, 140, 0),
    6: (255, 0, 0),
    7: (140, 0, 140),
    8: (0, 0, 255),
    9: (0, 100, 100),
    10: (0, 255, 0),
    11: (100, 100, 0),
    12: (255, 0, 0),
    13: (100, 0, 100)
}

SYMBOLS = {
    -1: "1",
    0: "Board8",
    1: "Board6",
    2: "K",
    3: "Q",
    4: "R",
    5: "B",
    6: "N",
    7: "P",
    8: "k",
    9: "q",
    10: "r",
    11: "b",
    12: "n",
    13: "p"
}

NAMES = {
    0: "Board8",
    1: "Board6",
    2: "WhiteKing",
    3: "WhiteQueen",
    4: "WhiteRook",
    5: "WhiteBishop",
    6: "WhiteKnight",
    7: "WhitePawn",
    8: "BlackKing",
    9: "BlackQueen",
    10: "BlackRook",
    11: "BlackBishop",
    12: "BlackKnight",
    13: "BlackPawn"
}

PIECES = {
    "Board8": "Board6",
    "Board6": "Board6",
    "K": "WhiteKing",
    "Q": "WhiteQueen",
    "R": "WhiteRook",
    "B": "WhiteBishop",
    "N": "WhiteKnight",
    "P": "WhitePawn",
    "k": "BlackKing",
    "q": "BlackQueen",
    "r": "BlackRook",
    "b": "BlackBishop",
    "n": "BlackKnight",
    "p": "BlackPawn",
}

CLASSES = {
    "Board8": 0,
    "Board6": 1,
    "K": 2,
    "Q": 3,
    "R": 4,
    "B": 5,
    "N": 6,
    "P": 7,
    "k": 2,
    "q": 3,
    "r": 4,
    "b": 5,
    "n": 6,
    "p": 7
}

NUMBERS = {
    "Board8": 0,
    "Board6": 1,
    "K": 2,
    "Q": 3,
    "R": 4,
    "B": 5,
    "N": 6,
    "P": 7,
    "k": 8,
    "q": 9,
    "r": 10,
    "b": 11,
    "n": 12,
    "p": 13
}

AMOUNT = {
    'Board8': [0, 1],
    'Board6': [0, 1],
    'K': [0, 1],
    'Q': [0, 3],
    'R': [0, 2],
    'B': [0, 2],
    'N': [0, 3],
    'P': [0, 8],
    'k': [0, 1],
    'q': [0, 3],
    'r': [0, 2],
    'b': [0, 2],
    'n': [0, 3],
    'p': [0, 8],
}
